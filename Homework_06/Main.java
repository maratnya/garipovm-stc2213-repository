package Homework_06;

/*
6. Алгоритмы
        На вход подается последовательность чисел, стремящаяся к бесконечности (последовательность может быть и из 100 чисел, может и их 1000000 чисел быть), оканчивающаяся на -101.
        Необходимо вывести число, которое присутствует в последовательности минимальное количество раз.
        Условия задачи:


        Все числа в диапазоне от -100 до 100.
        Числа встречаются не более 2 147 483 647-раз каждое.
        Сложность алгоритма - O(n)
*/


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //printArray();

        // для Легкости проверки выполнения условий задачи создадим временный массив:
        int[] array = {-5, -5,-1,-1,1,-1,3,5,2,2,2,3, 34,34,5,2,1,-100,-32,-100,32};
        // Функция для ручного заполнения массива:
        // printArray(array);

        System.out.print("Числа, которые встречаются в последовательности минимальное количество раз:");
        frequency(array);
    }

    // Создаем функцию для заполнения массива
    public static void addArray () {
        ArrayList<Integer> array = new ArrayList<>(); // объявили динамический массив
        Scanner scanner = new Scanner(System.in); // объявили объект scanner, который будет получать значения с клавиатуры

        int number = scanner.nextInt();

        while (number != -101){ // Условие для остановки заполнения
            array.add(number);
            number = scanner.nextInt();
        }
        String inArrayString = Arrays.toString(new ArrayList[]{array});
        System.out.println(inArrayString);
    }
    // Функция для заполнения массива
    public static void printArray (int[] array) {
        for(int i = 0; i < array.length; i++ ){
            System.out.print(array[i] + ",");
        }
    }
    // Функция для поиска числа, которые встречаются в последовательности минимальное количество раз
    public static void frequency (int[] array) {
        int[] tempArray = new int[201];
        int tempNumber;
        //Перебираем массив и создаем новый с количеством повторений
        for(int i = 0; i < array.length; i++){
            tempNumber = array[i] + 100;
            tempArray[tempNumber] ++;
        }

        int temp = 1;
        // Перебираем ранне созданный массив с частотой встречаемости чисел
        for(int i = 0; i < tempArray.length; i++){
            if (temp == tempArray[i]) {
                System.out.print(i - 100 + ",");
            }
        }
    }
}
