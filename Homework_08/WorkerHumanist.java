package Homework_08;

public class WorkerHumanist extends Worker{

    public WorkerHumanist(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Работает воображением.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Во время отпуска едет в исторические места.");
    }
}
