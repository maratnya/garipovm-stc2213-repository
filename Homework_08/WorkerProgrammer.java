package Homework_08;

public class WorkerProgrammer extends Worker{

    public WorkerProgrammer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Работает головой.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Во время отпуска занимается саморазвитием.");
    }
}
