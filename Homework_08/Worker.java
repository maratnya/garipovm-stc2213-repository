package Homework_08;

public class Worker {

    private String name; // имя
    private String lastName; // фамилия
    private String profession; // профессия

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }



    public Worker(String name, String lastName, String profession) {
        setName(name);
        setLastName(lastName);
        setProfession(profession);
    }

    public void goToWork() {
        System.out.println("****************************");

        System.out.println(lastName + " " + name);
        System.out.println("Профессия: " + profession + ".");
        //System.out.println("Работает добросовестно");
    }

    public void goToVacation(int days){

        System.out.println(lastName + " " + name + " уходит в отпуск на " + days + " дней.");
        //System.out.println("Во время отпуска он поедет отдыхать в Дубай.");
    }
}
