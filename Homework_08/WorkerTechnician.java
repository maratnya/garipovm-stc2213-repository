package Homework_08;

public class WorkerTechnician extends Worker{

    public WorkerTechnician(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Работает головой и руками.");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Во время отпуска отдыхает на даче.");
    }
}
