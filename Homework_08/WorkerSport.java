package Homework_08;

public class WorkerSport extends Worker {

    public WorkerSport(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Работает над телом.");;
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("Во время отпуска занимется бегом.");
    }
}
