package Homework_09;/*
9. Абстрактные классы и интерфейсы

Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть абстрактный метод getPerimeter().
Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит перемещать фигуру на заданные координаты.
Данный интерфейс должны реализовать только классы Circle и Square.
В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр, а у "перемещаемых" фигур изменить случайным образом координаты.
*/

import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random(); // Для перемещения фигур на случайно заданные координаты

        Rectangle rectangle = new Rectangle(5,2); // У прямоугольника задается вершина, центр расположен в (0,0)
        Square square = new Square(5,5);
        Ellipse ellipse = new Ellipse(4,5); // У эллипса задаем точки пересечения с координатной осью, т.е - (x, 0) и (0, у)
        Circle circle = new Circle(5,5); 

        rectangle.getPerimeter();
        ellipse.getPerimeter();

        circle.getPerimeter();
        circle.move(random.nextInt(10), random.nextInt(10) );

        square.getPerimeter();
        square.move(random.nextInt(10), (random.nextInt(10)) );

    }
}
