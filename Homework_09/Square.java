package Homework_09;

public class Square extends Rectangle implements Moveable{
    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        super.getPerimeter();
        System.out.println();
        System.out.println( "* Данный прямоугольник является квадратом." );
    }

    @Override
    public void move(int x, int y) {
        System.out.println();
        System.out.println("* Перемещаем центр квадрата на заданные координаты: (" + x + "," + y + ")");
        System.out.println("    Новый координаты вершины квадрата: (" + (getX() + x) + "," + (getY() + y) + ").");
    }
}
