package Homework_09;

public class Circle extends Ellipse implements Moveable{

    public Circle(int x, int y) {
        super(x, y);
    }
    @Override
    public void getPerimeter() {
        super.getPerimeter();
        System.out.println();
        System.out.println("* Данный эллипс является кругом.");
    }

    @Override
    public void move(int x, int y) {
        System.out.println();
        System.out.println("* Перемещаем центр круга на заданные координаты: (" + x + "," + y + ")");
        System.out.println("    Новый координаты круга: ");
        System.out.println("        - первая точка (" + (getX() + x) + "," + y + ");");
        System.out.println("        - вторая точка (" + x + "," + (getY() + y) + ").");
    }
}
