package Homework_09;

public class Ellipse extends Figure {

    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println();
        System.out.println("############ ЭЛЛИПС ############");
        System.out.println("* Построен Эллипс по трем точкам:");
        System.out.println("    - центр (0,0)");
        System.out.println("    - первая точка (" + getX() + ",0);");
        System.out.println("    - вторая точка (0," + getY() + ").");
        System.out.println();
        System.out.println("* Периметр данного эллипса равняется: " +
                (2 * 3.14 * Math.sqrt(((getX() * getX()) + (getY() * getY())) * 0.5 )) + ".");
    }
}
