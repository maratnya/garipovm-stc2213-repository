package Homework_09;

public class Rectangle extends Figure{

    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println();
        System.out.println("############ ПРЯМОУГОЛЬНИК ############");
        System.out.println("* Построен прямоугольник по двум точкам:");
        System.out.println("    - центр прямоугольника: (0,0);");
        System.out.println("    - вершина прямоугольника: (" + getX() + "," + getY() + ").");
        System.out.println();
        System.out.println("* Периметр данного прямоугольника равняется: " + (getX()+getY())*4 + ".");
    }
}

