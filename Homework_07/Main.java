package Homework_07;/* Создать класс Human используя инкапсуляцию (модификаторы и методы доступа), у которого будут поля:

String name
String lastName
int age

Будет два конструктора - один пустой, второй - полный (инициализирует все поля)

Создать класс Main, в котором будет создаваться массив на случайное количество человек.
После инициализации массива - заполнить его объектами класса Human (у каждого объекта случайное
значение поля age). После этого - отсортировать массив по возрасту и вывести результат в консоль
*/

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        int size = random.nextInt(110);

        Human[] humans = new Human[size];

        // заполнение массива данными
        for(int i = 0; i < humans.length; i ++){

            humans[i] = new Human("Петр" + i, "Петров" + i, random.nextInt(100));
        }

        // Сортировка массива по возросту bubbleSort
        for(int i = 0; i < humans.length; i++) {
            for ( int j = 0; j < humans.length - 1; j ++ ) {
                if (humans[j].getAge() > humans[j+1].getAge()){
                    Human temp = new Human(humans[j].getName(), humans[j].getLastName(), humans[j].getAge());
                    humans[j] = humans [j+1];
                    humans [j+1] = temp;
                }
            }
        }

        // печать массива
        for(int i = 0; i < humans.length; i++ ) {
            humans[i].printHuman();
        }

    }
}
