package Homework_07;

public class Human {

    //создание скрытых переменных через модификаторы
    private String name;
    private String lastName;
    private int age;

    //создание пустого конструктора
    public Human () {}

    //создание полного конструктора
    public Human (  String name,
                    String lastName,
                    int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public void printHuman (){
        System.out.println("Фамилия и имя: " + lastName + " " + name + ". Возраст: " + age + "." );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
