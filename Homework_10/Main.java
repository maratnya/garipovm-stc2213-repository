package Homework_10;
/*
10. Анонимныеи лямбда выражения классы

Предусмотреть функциональный интерфейс:

interface ByCondition {
boolean isOk(int number);
}

Написать класс Sequence, в котором должен присутствовать метод filter:
public static int[] filter(int[] array, ByCondition condition) {
}

Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.

В main в качестве condition подставить:
    * проверку на четность элемента
    * проверку, является ли сумма цифр элемента четным числом.

Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром, 12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).
*/

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();
        int[] array = new int[15];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000);
        }

        System.out.println();
        System.out.println("Исходный массив: " + Arrays.toString(array));
        System.out.println();

        Sequence sequence = new Sequence();

        System.out.print("Четные элементы массива: ");
        sequence.filter(array, temp -> temp % 2 == 0);

        System.out.print("Элементы массива у которых сумма цифр является четным: ");
        sequence.filter(array, temp ->{
            int sum = 0;
            while ( temp != 0) {
                sum = sum + temp % 10;
                temp = temp / 10;
            }
            return sum % 2 == 0;
        });

        System.out.print("Элементы массива у которых все цифры являются четным: ");
        sequence.filter(array, number ->{
            while ( number != 0) {
                if (number % 2 != 0) {
                    return false;
                }
                number = number / 10;
            }
            return true;
        });

        System.out.print("Элементы массива которые являются палиндромами: ");
        sequence.filter(array, number ->{

            // Создаем цикл для записи числа наоборот
            int temp = 0;
            int tempA = number;
            while (tempA !=0 ) {
                temp = temp * 10 + tempA % 10;
                tempA = tempA / 10;
            }

            // палиндромные числа одинаково читаются в обоих направлениях:
            if (temp == number) {
                return true;
            }
            return false;
        });
    }



}
