package Homework_10.v2;

import java.util.ArrayList;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {

        ArrayList<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])){
                arrayList.add(array[i]);
            }
        }

        //преобразуем ArrayList в int[]
        return arrayList.stream().mapToInt(i -> i).toArray();
    }
}
