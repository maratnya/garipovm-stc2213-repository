package Homework_05;

/*
5. Функции и процедуры
Реализовать функцию, принимающую на вход массив и целое число.
Данная функция должна вернуть индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

Было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
Стало после применения процедуры:
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
*/

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int lenghtArray;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        lenghtArray = scanner.nextInt();
        int[] array = new int[lenghtArray];

        System.out.println("Введите элементы массива: ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Поиск индекса числа: ");
        int number = scanner.nextInt();

        int a = -1;
        for (int i = 0; i < array.length; i++) {
            if( array[i] == number ) {
                a = i;
            }
        }

        System.out.println(a);

        System.out.println("Перемещение всех значимых элементов влево, заполнив нулевые:");

        a = array.length - 1;
        int j = 0;
        int[] temp;
        temp = new int[array.length];

        for(int i = 0; i < array.length; i++ ) {
            if (array[i] != 0 ) {
                temp[j] = array[i];
                j++;
            } else {
                temp[a] = 0;
                a--;
            }
        }

        String intTempArrayString = Arrays.toString(temp);
        System.out.println(intTempArrayString);


    }

}
