package dop1;

public class Mouse extends Good{
    private String color;

    public Mouse(String name, int price, String color) {
        super(name, price);
        setColor(color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Название мышки: " + getName() + "\nЦвет мышки: " + getColor() + "\nЦена мышки: " + getPrice();
    }
}
