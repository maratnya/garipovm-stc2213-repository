package dop1;

public class Main {

    public static void main(String[] args) {
        Good[] goods = new Good[3];
        goods[0] = new Display("ACER", 10000, 27);
        goods[1] = new Keyboard("DEXP", 500, 87);
        goods[2] = new Mouse("A4TECH", 200, "White");

        //System.out.println(Discounter.getPriceWithDiscont(goods[0], 25.0));
        //System.out.println(Discounter.getPriceWithDiscont(goods[1], 10.0));
        //System.out.println(Discounter.getPriceWithDiscont(goods[2], 15.0));

        double discount = 20;

        Discounter.printObject(goods[0], discount);
        Discounter.printObject(goods[1], discount);
        Discounter.printObject(goods[2], discount);
    }
}
