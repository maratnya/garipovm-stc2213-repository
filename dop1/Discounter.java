package dop1;

public class Discounter {

    public static <T extends Good> double getPriceWithDiscont(T object, double discount){
        return object.getPrice() * ((100-discount)/100);
    }

    public static <T extends Good> void printObject (T object, double discount) {

        //double temp = getPriceWithDiscont(object, discount);
        System.out.println("******************************************");
        System.out.println(object.toString());
        System.out.println("Цена с учетом скидки в " + discount + "%: " + getPriceWithDiscont(object, discount));
        System.out.println("******************************************");
        System.out.println();

    }
}
//Добавить метод, который будет принимать массив любых объектов и вызывать у них метод toString()
//toString переопределить в остальных классах
