package dop1;

public class Display extends Good{

    private int size;

    public Display(String name, int price, int size) {
        super(name, price);
        setSize(size);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Название дисплея: " + getName() + "\nРазмер дисплея: " + getSize() + "\nЦена дисплея: " + getPrice();
    }

}
