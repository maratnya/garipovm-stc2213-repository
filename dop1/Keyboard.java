package dop1;

public class Keyboard extends Good{

    private int keysCount;

    public Keyboard(String name, int price, int keysCount) {
        super(name, price);
        setKeysCount(keysCount);
    }

    public int getKeysCount() {
        return keysCount;
    }

    public void setKeysCount(int keysCount) {
        this.keysCount = keysCount;
    }

    @Override
    public String toString() {
        return "Название клавиатуры: " + getName() + "\nКоличество клавиш: " + getKeysCount() + "\nЦена клавиатуры: " + getPrice();
    }
}
