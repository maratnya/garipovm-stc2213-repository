package Attestation.Var1;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class UsersRepositoryFileImpl {

    private static String nameFile = "Attestation\\Var1\\users.txt";
    public static Map<Integer, User> readFile (String nameFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(nameFile)))){
            Map<Integer, User> userMap = new HashMap<>();
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] userList = line.split("\\|");
                User user = new User(Integer.parseInt(userList[0]), userList[1], userList[2], Integer.parseInt(userList[3]), Boolean.parseBoolean(userList[4]));
                userMap.put(user.getId(), user);
            }
            return userMap;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    } // Чтение файла, возвращает Map
    public static void writeFile (Map<Integer, User> userMap) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(nameFile)))){
            for (User user : userMap.values()) {
                writer.write(user.toString() + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    } // Запись файла
    public static User findById(int id) {
        Map<Integer, User> userMap = readFile(nameFile);
        return userMap.get(id);
    } //Метод findById возвращает user с id
    public static void create(User user) {
        Map<Integer, User> userMap = readFile(nameFile);
        if(userMap.containsKey(user.getId())) {
            System.out.println("Пользователь с ID = " + user.getId() + " уже существует");
        } else {
            userMap.put(user.getId(), user);
            writeFile(userMap);
            System.out.println( "Пользователь с ID = " + user.getId() + " успешно добавлен!" );
        }
    }    // Метод create записывает User в файл
    public static void update(User user) {
        Map<Integer, User> userMap = readFile(nameFile);
        userMap.put(user.getId(), user);
        writeFile(userMap);
    }     // Метод update обновляет данные пользователя User в файле
    public static void delete(int id) {
        Map<Integer, User> userMap = readFile(nameFile);
        if(userMap.containsKey(id)){
            userMap.remove(id);
            writeFile(userMap);
            System.out.println("Пользователь успешно удален!");
        } else {
            System.out.println("Пользователя с ID = " + id + " не существует!");
        }
    } // Метод delete удаляет пользователя с id
}
