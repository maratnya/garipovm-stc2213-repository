package Attestation.Var3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        UsersRepositoryFileImpl usersRepo = new UsersRepositoryFileImpl();

        try (BufferedReader reader = new BufferedReader(new FileReader(new File("Attestation\\Var3\\users.txt")))){
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] userList = line.split("\\|");
                User user = new User(Integer.parseInt(userList[0]), userList[1], userList[2], Integer.parseInt(userList[3]), Boolean.parseBoolean(userList[4]));
                usersRepo.create(user);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        User user = new User(13,"user", "user", 14, true);
        System.out.println("Создание пользователя: " + user.toString());
        usersRepo.create(user);

        System.out.println();

        int id = 13;
        user = usersRepo.findById(id);
        System.out.print("Пользователь с ID = " + id + ": ");
        try {
            System.out.println(user.toString());
        } catch (NullPointerException e) {
            System.out.println (" отсутствует в списке!");
        }

        user = usersRepo.findById(3);
        user.setName("Marat");
        user.setAge(24);
        usersRepo.update(user);

        System.out.println();
        usersRepo.delete(13);
    }
}
