package Attestation.Var3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UsersRepositoryFileImpl {
    private Map<Integer, User> userMap;

    public UsersRepositoryFileImpl() {
        userMap = new HashMap<>();
    }
    public static void writeFile (Map<Integer, User> userMap) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File("Attestation\\Var3\\users.txt")))){
            for (User user : userMap.values()) {
                writer.write(user.toString() + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    } // Запись файла
    public User findById(int id) {
        return userMap.get(id);
    } //Метод findById возвращает user с id
    public void create(User user) {
        if(userMap.containsKey(user.getId())) {
            System.out.println("Пользователь с ID = " + user.getId() + " уже существует");
        } else {
            userMap.put(user.getId(), user);
            writeFile(userMap);
            //System.out.println( "Пользователь с ID = " + user.getId() + " успешно добавлен!" );
        }
    }    // Метод create записывает User в файл
    public void update(User user) {
        userMap.put(user.getId(), user);
        writeFile(userMap);
    }     // Метод update обновляет данные пользователя User в файле
    public void delete(int id) {
        if(userMap.containsKey(id)){
            userMap.remove(id);
            writeFile(userMap);
            System.out.println("Пользователь с ID = " + id + " успешно удален!");
        } else {
            System.out.println("Пользователя с ID = " + id + " не существует!");
        }
    } // Метод delete удаляет пользователя с id
}
