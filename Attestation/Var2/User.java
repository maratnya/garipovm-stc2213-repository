package Attestation.Var2;

import java.util.Objects;

public class User {

    private int id;
    private String name;
    private String surname;
    private int age;

    private Boolean work;

    public User(int id, String name, String surname, int age, Boolean work) {
        setId(id);
        setName(name);
        setSurname(surname);
        setAge(age);
        setWork(work);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getWork() {
        return work;
    }

    public void setWork(Boolean work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + surname + '|' + age + "|" + work + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
