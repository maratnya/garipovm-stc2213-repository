package Attestation.Var2;

public class Main {
    public static void main(String[] args) {

        User user = new User(5,"user", "user", 14, true);
        System.out.print("Создание пользователя: " + user.toString());
        UsersRepositoryFileImpl.create(user);

        System.out.println();

        int id = 10;
        user = UsersRepositoryFileImpl.findById(id);
        System.out.print("Пользователь с id = " + id + ": ");
        try {
            System.out.println(user.toString());
        } catch (NullPointerException e) {
            System.out.println (" отсутствует в списке!");
        }
        System.out.println();


        // выбор пользователя для внесения изменений
        user = UsersRepositoryFileImpl.findById(3);
        user.setName("Marat");
        user.setAge(24);
        UsersRepositoryFileImpl.update(user);

        System.out.println();
        UsersRepositoryFileImpl.delete(4);


    }
}
