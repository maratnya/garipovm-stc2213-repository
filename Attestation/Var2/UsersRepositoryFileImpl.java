package Attestation.Var2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl {
    private static String nameFile = "Attestation\\Var2\\users.txt";
    private static List<User> readerFile () {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(nameFile)))){
            List<User> userList = reader.lines().map(line -> {
                String[] temp = line.split("\\|");
                return new User(Integer.parseInt(temp[0]), temp[1], temp[2], Integer.parseInt(temp[3]), Boolean.parseBoolean(temp[4]));
            }).collect(Collectors.toList());
            return userList;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    } // Чтение из файл
    private static void writerFile (List<User> userList) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(nameFile)))){
            for (User user : userList) {
                writer.write(user.toString());
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
    } // Запись в файл
    public static User findById(int id) {
        List<User> userList = readerFile();
        for (User user : userList) {
            if(user.getId() == id){
                return user;
            }
        }
        return null;
    } //Метод findById возвращает user с id
    public static void create(User user) {
        List<User> userList = readerFile();
        List<Integer> number = new ArrayList<>();
        for (User userT : userList) {
            number.add(userT.getId());
        }
        if(number.contains(user.getId())) {
            System.err.println("Пользователь с ID = " + user.getId() + " уже существует");
        } else {
            userList.add(user);
            System.out.println("Пользователь успешно добавлен в список!");
            writerFile(userList);
        }

    } // Метод create проверяет наличие в файле пользователя с данным ID и при отсутствии записывает User в файл
    public static void update(User user) {
        List<User> userList = readerFile();
        List<Integer> number = new ArrayList<>();
        int a = 0;
        for (User userT : userList) {
            number.add(userT.getId());
            while (!number.contains(user.getId())){
                a ++;
                if(a == number.size()) {
                    break;
                }
            }
        }
        if (number.contains(user.getId())){
            userList.set(a, user);
            writerFile(userList);
            System.out.println("Данные пользователя с ID = " + user.getId() + " успешно обновлены!");
        } else {
            System.err.println("Пользователя с ID = " + user.getId() + " не существует!");
        }
    } // обновление данных пользователя
    public static void delete (int id) {

        List<User> userList = readerFile();
        List<Integer> number = new ArrayList<>();
        for (User userT : userList) {
            number.add(userT.getId());
        }
        if(number.contains(id)) {
            try (BufferedReader reader = new BufferedReader(new FileReader(new File(nameFile)))){
                List<User> userListTemp = new ArrayList<>();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    String[] temp = line.split("\\|");
                    User user = new User(Integer.parseInt(temp[0]), temp[1], temp[2], Integer.parseInt(temp[3]), Boolean.parseBoolean(temp[4]));
                    if(user.getId() != id) {
                        userList.add(user);
                    }
                }
                writerFile(userList);
                System.out.println("Пользователь c ID = " + id + "успешно удален!");
            } catch (IOException e) {
                throw new RuntimeException(e);
            };
        } else {
            System.out.println("Пользователь  c ID = " + id + " отсутствует в списке!");
            writerFile(userList);
        }
    } // удаление пользователя
}
