/*
    12. Collections

На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
Вывести:
    Слово - количество раз
    Слово - количество раз\
Использовать Map, у объектов String есть метод для деления строки на части по определенному символу. Слово - символы, ограниченные пробелами справа и слева.

Пример:
    Маша и три медведя

    Маша - 1 раз
    и - 1 раз
    три - 1 раз
    медведя - 1 раз
*/
package Homework_12;

import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        // Используем LinkedHashMap для упорядоченного вывода информации
        Map<String, Integer> message = new LinkedHashMap<>();
        String text = "Маша и три медведя";
        String[] temp = text.split(" ");

        for (int i = 0; i < temp.length; i++) {
            if (message.containsKey(temp[i])){
                int tempN = message.get(temp[i]);
                tempN++;
                message.put(temp[i],tempN);
            }  else {
                message.put(temp[i], 1);
            }
        }

        for (Map.Entry<String, Integer> messageTemp: message.entrySet()){
            System.out.println(messageTemp.getKey() + " - " + messageTemp.getValue() + " раз");
        }
    }
}
