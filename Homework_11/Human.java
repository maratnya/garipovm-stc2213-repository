package Homework_11;

import java.util.Objects;

public class Human {

    private String name;
    private String lastName;
    private String patronymic; // Отчество
    private String city;
    private String street;
    private String house;
    private String flat; // Квартира
    private String numberPassport;

    public Human(String name, String lastName, String patronymic, String city, String street, String house, String flat, String numberPassport) {
        setName(name);
        setLastName(lastName);
        setPatronymic(patronymic);
        setCity(city);
        setStreet(street);
        setHouse(house);
        setFlat(flat);
        setNumberPassport(numberPassport);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    /*  Пупкин Вася Варфаламеевич
        Паспорт:
        Серия: 98 22 Номер: 897643
        Город Джава, ул. Программистов, дом 15, квартира 54
        Метод equals() должен сравнивать людей по номеру паспорта
    */

    @Override
    public String toString() {
        return "================================================== \n" +
                getLastName() + " " + getName() + " " + getPatronymic() + " \n" +
                "Паспорт: \n" +
                "Серия: " + getNumberPassport().substring(0,5) + " Номер: " + getNumberPassport().substring(6) +
                "\nГород " + getCity() + ", ул. " + getStreet() + ", дом " + getHouse() + ", квартира " + getFlat() +
                "\n==================================================\n";
    }

/*
    Метод equals() должен сравнивать людей по номеру паспорта
*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return numberPassport.equals(human.numberPassport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numberPassport);
    }
}
