package Homework_11;

/*
11. Object and String

Создать класс Human у которого будут поля:

    private String name;
    private String lastName;
    private String patronymic;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String numberPassport;

Переопределить три метода: toString(), hashCode(), equals

Метод toString должен выводить информацию таким образом (пример):
Пупкин Вася Варфаламеевич
Паспорт:
Серия: 98 22 Номер: 897643
Город Джава, ул. Программистов, дом 15, квартира 54
Метод equals() должен сравнивать людей по номеру паспорта
*/

public class Main {
    public static void main(String[] args) {


        Human human1 = new Human("Иван",
                "Иванов",
                "Иванович",
                "Казань",
                "Вишневского",
                "54",
                "2",
                "92 42 367324");

        Human human2 = new Human("Петр",
                "Петров",
                "Петрович",
                "Казань",
                "Толстого",
                "32",
                "4",
                "94 34 542231");

        Human human3 = new Human("Василий",
                "Васильев",
                "Васильевич",
                "Казань",
                "Толстого",
                "34",
                "4",
                "95 44 654456");

        Human human4 = new Human("Иван",
                "Петров",
                "Васильевич",
                "Москва",
                "Вишневского",
                "2",
                "5",
                "99 35 534612");

        Human human5 = new Human("Петр",
                "Иванов",
                "Васильевич",
                "Москва",
                "Толстого",
                "11",
                "9",
                "92 42 367324");

        System.out.println(human1.equals(human2));
        System.out.println(human2.equals(human1));
        System.out.println(human1.equals(human3));
        System.out.println(human1.equals(human4));
        System.out.println(human1.equals(human5));
        System.out.println(human5.equals(human1));
        System.out.println();


        System.out.println(human1.toString());
        System.out.println(human5.toString());


    }
}
